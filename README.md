# File Extractor

This module adds a new computed field on File entity: `File extractor: extracted
file`.

This new field allows to access the content of the file:
- in web services like JSON:API
- in a field formatter (file field)
- in [Search API](https://www.drupal.org/project/search_api)

The module provides the following extraction methods:
- Docconv binary
- Pdftotext binary
- Python Pdf2txt binary
- Solr built-in extractor ([Search API Solr](https://www.drupal.org/project/search_api_solr))
- Tika App JAR
- Tika Server JAR


## Requirements

This module requires no modules outside of Drupal core.

Each extractor plugin can require different modules or libraries, if the
requirements are not satisfied the plugin doesn't show up in the settings.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

Go to the configuration page (`/admin/config/media/file-extractor`) and
configure the extraction settings.

The module provides its own cache bin `file_extractor`, so in your
`settings.php` file you can override the cache backend for this cache bin. For
example if you want to use the
[File Cache](https://www.drupal.org/project/filecache) module:

```php
$settings['cache']['bins']['file_extractor'] = 'cache.backend.file_system';
```
