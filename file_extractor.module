<?php

/**
 * @file
 * Contains hook implementations for file_extractor module.
 */

declare(strict_types=1);

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Routing\RouteMatchInterface;
use Drupal\file_extractor\HookHandler\EntityTypeInfoHandler;

/**
 * Implements hook_help().
 */
function file_extractor_help(string $route_name, RouteMatchInterface $route_match): string {
  $text = '';

  switch ($route_name) {
    case 'help.page.file_extractor':
      $text = \file_get_contents(__DIR__ . '/README.md');
      if (!$text) {
        return '';
      }
      if (!\Drupal::moduleHandler()->moduleExists('markdown')) {
        return '<pre>' . $text . '</pre>';
      }

      // Use the Markdown filter to render the README.
      $filter_manager = \Drupal::service('plugin.manager.filter');
      $settings = \Drupal::configFactory()->get('markdown.settings')->getRawData();
      $config = ['settings' => $settings];
      $filter = $filter_manager->createInstance('markdown', $config);
      // @phpstan-ignore-next-line
      return $filter->process($text, 'en');
  }

  return $text;
}

/**
 * Implements hook_entity_base_field_info().
 */
function file_extractor_entity_base_field_info(EntityTypeInterface $entity_type): array {
  /** @var \Drupal\file_extractor\HookHandler\EntityTypeInfoHandler $instance */
  $instance = \Drupal::service('class_resolver')
    ->getInstanceFromDefinition(EntityTypeInfoHandler::class);
  return $instance->entityBaseFieldInfo($entity_type);
}
