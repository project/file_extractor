<?php

declare(strict_types=1);

namespace Drupal\file_extractor\HookHandler;

use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\file_extractor\Plugin\Field\ExtractedFileFieldItemList;

/**
 * Manipulates entity type information.
 */
class EntityTypeInfoHandler {

  use StringTranslationTrait;

  /**
   * Adds base field info to an entity type.
   *
   * @param \Drupal\Core\Entity\EntityTypeInterface $entity_type
   *   Entity type for adding base fields to.
   *
   * @return \Drupal\Core\Field\BaseFieldDefinition[]
   *   New fields added.
   *
   * @see hook_entity_base_field_info()
   */
  public function entityBaseFieldInfo(EntityTypeInterface $entity_type) {
    $fields = [];
    if ($entity_type->id() != 'file') {
      return $fields;
    }

    $fields['file_extractor_extracted_file'] = BaseFieldDefinition::create('string_long')
      ->setLabel($this->t('File extractor: extracted file'))
      ->setDescription($this->t('The value obtained after extraction of this file.'))
      ->setComputed(TRUE)
      ->setClass(ExtractedFileFieldItemList::class)
      ->setTranslatable(FALSE);

    return $fields;
  }

}
