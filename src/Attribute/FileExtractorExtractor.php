<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Attribute;

use Drupal\Component\Plugin\Attribute\Plugin;
use Drupal\Core\StringTranslation\TranslatableMarkup;

/**
 * Defines a FileExtractorExtractor attribute for plugin discovery.
 *
 * @ingroup plugin_api
 */
#[\Attribute(\Attribute::TARGET_CLASS)]
class FileExtractorExtractor extends Plugin {

  /**
   * Constructs a FileExtractorExtractor attribute.
   *
   * @param string $id
   *   The plugin ID.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $label
   *   (optional) The human-readable name of the File Extractor.
   * @param \Drupal\Core\StringTranslation\TranslatableMarkup|null $description
   *   (optional) A short description of the File Extractor.
   * @param string[] $dependencies
   *   (optional) The name of modules that are required for this plugin to be
   *   usable.
   * @param string[] $packageDependencies
   *   (optional) The name of Composer packages that are required for this
   *   plugin.
   */
  public function __construct(
    public readonly string $id,
    public readonly ?TranslatableMarkup $label = NULL,
    public readonly ?TranslatableMarkup $description = NULL,
    public readonly array $dependencies = [],
    public readonly array $packageDependencies = [],
  ) {}

}
