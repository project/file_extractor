<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Plugin annotation object.
 *
 * @ingroup plugin_api
 *
 * @Annotation
 */
class FileExtractorExtractor extends Plugin {

  /**
   * The plugin id.
   *
   * @var string
   */
  public $id;

  /**
   * The plugins label.
   *
   * @var string
   */
  public $label;

  /**
   * The plugin description.
   *
   * @var string
   */
  public $description;

  /**
   * The name of modules that are required for this plugin to be usable.
   *
   * @var array
   */
  public $dependencies;

  /**
   * The name of Composer packages that are required for this plugin.
   *
   * @var array
   */
  public $packageDependencies;

}
