<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Service;

use Drupal\file\FileInterface;

/**
 * Test file helper interface methods.
 */
interface TestFileHelperInterface {

  /**
   * Name of the file used for testing.
   */
  public const TEST_FILENAME = 'file_extractor $ test_extrāction ☺.pdf';

  /**
   * URI of the file used for testing.
   */
  public const TEST_FILE_URI = 'temporary://' . self::TEST_FILENAME;

  /**
   * Helper function to get the test file.
   *
   * @return \Drupal\file\FileInterface
   *   The file used for the test.
   */
  public function getTestFile(): FileInterface;

  /**
   * Helper function to delete the test file.
   */
  public function deleteTestFile(): void;

}
