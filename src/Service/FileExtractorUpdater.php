<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\file_extractor\Form\SettingsForm;

/**
 * Service to have better structured code for updates.
 */
class FileExtractorUpdater implements FileExtractorUpdaterInterface {

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * The module handler service.
   *
   * @var \Drupal\Core\Extension\ModuleHandlerInterface
   */
  protected ModuleHandlerInterface $moduleHandler;

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager service.
   * @param \Drupal\Core\Extension\ModuleHandlerInterface $module_handler
   *   The module handler service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    EntityTypeManagerInterface $entity_type_manager,
    ModuleHandlerInterface $module_handler,
  ) {
    $this->configFactory = $config_factory;
    $this->entityTypeManager = $entity_type_manager;
    $this->moduleHandler = $module_handler;
  }

  /**
   * {@inheritdoc}
   */
  public function update9101(): void {
    $this->update9101GlobalConfig();
    $this->update9101ViewModes();
    $this->update9101Views();
    if ($this->moduleHandler->moduleExists('layout_builder')) {
      $this->update9101LayoutBuilder();
      // Layout builder overrides. Into a hook_post_update.
    }
  }

  /**
   * Update 9101: global config.
   */
  protected function update9101GlobalConfig(): void {
    $file_extractor_config = $this->configFactory->getEditable(SettingsForm::CONFIG_NAME);
    /** @var array $file_extractor_settings */
    $file_extractor_settings = $file_extractor_config->get('extraction_settings');
    $file_extractor_config->set('extraction_settings', $this->update9101PrepareNewSettings($file_extractor_settings));
    $file_extractor_config->save();
  }

  /**
   * Update 9101: view modes.
   */
  protected function update9101ViewModes(): void {
    foreach ($this->configFactory->listAll('field.field.') as $field_config_name) {
      $field_config = $this->configFactory->getEditable($field_config_name);
      if ($field_config->get('field_type') != 'file') {
        continue;
      }

      $field_name = $field_config->get('field_name');

      foreach ($this->configFactory->listAll('core.entity_view_display.') as $view_display_name) {
        $view_display = $this->configFactory->getEditable($view_display_name);
        $field_display = $view_display->get('content.' . $field_name);
        if (\is_array($field_display) && $field_display['type'] == 'file_extractor_extracted_text') {
          $field_display['settings']['extraction_settings'] = $this->update9101PrepareNewSettings($field_display['settings']['extraction_settings']);
          $view_display->set('content.' . $field_name, $field_display);
          $view_display->save(TRUE);
        }
      }
    }
  }

  /**
   * Update 9101: views.
   */
  protected function update9101Views(): void {
    // As there is no way to know which view uses file fields with which
    // formatter, we need to loop on all views.
    foreach ($this->configFactory->listAll('views.view.') as $view_config_name) {
      $view = $this->configFactory->getEditable($view_config_name);
      /** @var array $displays */
      $displays = $view->get('display');
      $changed = FALSE;
      foreach ($displays as $key => $display) {
        if (empty($display['display_options']['fields'])) {
          continue;
        }

        /** @var array $display_options_fields */
        $display_options_fields = $display['display_options']['fields'];
        foreach ($display_options_fields as $field_name => $field) {
          // File field with File Extractor's formatter.
          if ($this->fieldIsFileField($field_name) && $field['type'] == 'file_extractor_extracted_text') {
            $field['settings']['extraction_settings'] = $this->update9101PrepareNewSettings($field['settings']['extraction_settings']);
            $displays[$key]['display_options']['fields'][$field_name] = $field;
            $changed = TRUE;
          }
        }
      }
      if ($changed) {
        $view->set('display', $displays);
        $view->save(TRUE);
      }
    }
  }

  /**
   * Update 9101: layout builder displays.
   */
  protected function update9101LayoutBuilder(): void {
    foreach ($this->configFactory->listAll('core.entity_view_display.') as $view_display_name) {
      $view_display = $this->configFactory->getEditable($view_display_name);
      $changed = FALSE;

      /** @var array $layout_builder_sections */
      $layout_builder_sections = $view_display->get('third_party_settings.layout_builder.sections');
      if (!$layout_builder_sections) {
        continue;
      }

      foreach ($layout_builder_sections as $key => $section) {
        foreach ($section['components'] as $uuid => $component_infos) {
          if (isset($component_infos['configuration']['formatter']['type']) && $component_infos['configuration']['formatter']['type'] == 'file_extractor_extracted_text') {
            $component_infos['configuration']['formatter']['settings']['extraction_settings'] = $this->update9101PrepareNewSettings($component_infos['configuration']['formatter']['settings']['extraction_settings']);
            $layout_builder_sections[$key]['components'][$uuid] = $component_infos;
            $changed = TRUE;
          }
        }
      }

      if ($changed) {
        $view_display->set('third_party_settings.layout_builder.sections', $layout_builder_sections);
        $view_display->save(TRUE);
      }
    }
  }

  /**
   * {@inheritdoc}
   */
  public static function update9101PrepareNewSettings(array $extractionSettings): array {
    $extractionSettings['extractable'] = [
      'excluded_extensions' => $extractionSettings['excluded_extensions'],
      'max_filesize' => $extractionSettings['max_filesize'],
      'exclude_private' => $extractionSettings['exclude_private'],
    ];
    $extractionSettings['extraction_result'] = [
      'number_first_bytes' => $extractionSettings['number_first_bytes'],
    ];

    unset($extractionSettings['excluded_extensions'], $extractionSettings['max_filesize'], $extractionSettings['exclude_private'], $extractionSettings['number_first_bytes']);

    return $extractionSettings;
  }

  /**
   * Check if a field machine name match a file field.
   *
   * @param string $field_name
   *   The field machine name to check for. it can be file_field_1.
   *
   * @return bool
   *   TRUE if the field is a file field, FALSE otherwise.
   */
  protected function fieldIsFileField(string $field_name): bool {
    $file_fields = $this->getFileFields();
    $check = FALSE;

    // Check if a file field is used.
    foreach ($file_fields as $file_field) {
      $pattern = '/^' . $file_field . '(_([\d])+)?$/';
      if (\preg_match($pattern, $field_name)) {
        $check = TRUE;
        break;
      }
    }

    return $check;
  }

  /**
   * Get all file fields' machine name.
   *
   * @return string[]
   *   The list of file fields' machine name.
   */
  protected function getFileFields(): array {
    /** @var string[]|null $file_fields */
    $file_fields = &\drupal_static(__FUNCTION__);

    if ($file_fields !== NULL) {
      /** @var \Drupal\field\Entity\FieldConfig[] $field_instance_config_entities */
      $field_instance_config_entities = $this->entityTypeManager->getStorage('field_config')->loadMultiple();

      $file_fields = [];
      foreach ($field_instance_config_entities as $field_instance_config_entity) {
        // Restrict to file fields.
        if ($field_instance_config_entity->get('field_type') == 'file') {
          /** @var string $field_name */
          $field_name = $field_instance_config_entity->get('field_name');

          // Check if we already have this field.
          if (!\in_array($field_name, $file_fields, TRUE)) {
            $file_fields[] = $field_name;
          }
        }
      }
    }

    return $file_fields ?? [];
  }

}
