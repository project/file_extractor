<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Service;

/**
 * File Extractor updater interface methods.
 */
interface FileExtractorUpdaterInterface {

  /**
   * Implementation of file_extractor_update_9101().
   */
  public function update9101(): void;

  /**
   * Prepare new settings structure.
   *
   * @param array $extractionSettings
   *   The extraction settings with the old structure.
   *
   * @return array
   *   The extraction settings with the new structure.
   */
  public static function update9101PrepareNewSettings(array $extractionSettings): array;

}
