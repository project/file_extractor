<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Service;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Extension\ModuleExtensionList;
use Drupal\Core\File\FileExists;
use Drupal\Core\File\FileSystemInterface;
use Drupal\file\FileInterface;

/**
 * Provides helper methods for manipulating test files.
 */
class TestFileHelper implements TestFileHelperInterface {

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * File system service.
   *
   * @var \Drupal\Core\File\FileSystemInterface
   */
  protected FileSystemInterface $fileSystem;

  /**
   * Module extension list.
   *
   * Currently no interface to rely on.
   *
   * @var \Drupal\Core\Extension\ModuleExtensionList
   */
  protected ModuleExtensionList $moduleExtensionList;

  /**
   * The test file.
   *
   * @var \Drupal\file\FileInterface|null
   */
  protected $testFile;

  /**
   * TestFileHelper constructor.
   *
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Core\File\FileSystemInterface $fileSystem
   *   The file system service.
   * @param \Drupal\Core\Extension\ModuleExtensionList $moduleExtensionList
   *   The module extension list.
   */
  public function __construct(
    EntityTypeManagerInterface $entityTypeManager,
    FileSystemInterface $fileSystem,
    ModuleExtensionList $moduleExtensionList,
  ) {
    $this->entityTypeManager = $entityTypeManager;
    $this->fileSystem = $fileSystem;
    $this->moduleExtensionList = $moduleExtensionList;
  }

  /**
   * {@inheritdoc}
   */
  public function getTestFile(): FileInterface {
    if (!isset($this->testFile)) {
      // Copy the source file to public directory.
      $source = $this->moduleExtensionList->getPath('file_extractor');
      $source .= '/data/' . self::TEST_FILENAME;
      $this->fileSystem->copy($source, self::TEST_FILE_URI, FileExists::Replace);

      /** @var \Drupal\file\FileInterface $file */
      $file = $this->entityTypeManager->getStorage('file')->create([
        'filename' => self::TEST_FILENAME,
        'filemime' => 'application/pdf',
        'uri' => self::TEST_FILE_URI,
        'status' => FileInterface::STATUS_PERMANENT,
      ]);
      $this->testFile = $file;
    }

    return $this->testFile;
  }

  /**
   * {@inheritdoc}
   */
  public function deleteTestFile(): void {
    $this->fileSystem->delete(self::TEST_FILE_URI);
    $this->testFile = NULL;
  }

}
