<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Service;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Utility\Bytes;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\file\FileInterface;
use Drupal\file_extractor\Event\FileIndexableEvent;
use Drupal\file_extractor\ExtractionSettingsFormHelper;
use Drupal\file_extractor\Extractor\ExtractorPluginManager;
use Drupal\file_extractor\Form\SettingsForm;
use Psr\Log\LoggerInterface;
use Symfony\Component\Mime\MimeTypeGuesserInterface;
use Symfony\Contracts\EventDispatcher\EventDispatcherInterface;

/**
 * Manager which provides methods to extract files.
 */
class ExtractorManager implements ExtractorManagerInterface {

  /**
   * Private scheme prefix length.
   */
  public const PRIVATE_SCHEME_PREFIX_LENGTH = 10;

  /**
   * The config factory service.
   *
   * @var \Drupal\Core\Config\ConfigFactoryInterface
   */
  protected ConfigFactoryInterface $configFactory;

  /**
   * The extractor plugin manager.
   *
   * @var \Drupal\file_extractor\Extractor\ExtractorPluginManager
   */
  protected ExtractorPluginManager $extractorPluginManager;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * The mime type guesser.
   *
   * @var \Symfony\Component\Mime\MimeTypeGuesserInterface
   */
  protected MimeTypeGuesserInterface $mimeTypeGuesser;

  /**
   * The event dispatcher service.
   *
   * @var \Symfony\Contracts\EventDispatcher\EventDispatcherInterface
   */
  protected EventDispatcherInterface $eventDispatcher;

  /**
   * The cache backend service.
   *
   * @var \Drupal\Core\Cache\CacheBackendInterface
   */
  protected CacheBackendInterface $cacheBackend;

  /**
   * Extraction settings.
   *
   * @var array
   */
  protected array $extractionSettings = [];

  /**
   * Store the calculated mime types for a list of extensions string.
   *
   * @var string[][]
   */
  protected array $mimesForExtensions = [];

  /**
   * Constructor.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The config factory service.
   * @param \Drupal\file_extractor\Extractor\ExtractorPluginManager $extractor_plugin_manager
   *   The extractor plugin manager.
   * @param \Psr\Log\LoggerInterface $logger
   *   Logger.
   * @param \Symfony\Component\Mime\MimeTypeGuesserInterface $mimeTypeGuesser
   *   Mime type guesser service.
   * @param \Symfony\Contracts\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   The event dispatcher service.
   * @param \Drupal\Core\Cache\CacheBackendInterface $file_extractor_cache
   *   The cache backend service.
   */
  public function __construct(
    ConfigFactoryInterface $config_factory,
    ExtractorPluginManager $extractor_plugin_manager,
    LoggerInterface $logger,
    MimeTypeGuesserInterface $mimeTypeGuesser,
    EventDispatcherInterface $event_dispatcher,
    CacheBackendInterface $file_extractor_cache,
  ) {
    $this->configFactory = $config_factory;
    $this->extractorPluginManager = $extractor_plugin_manager;
    $this->logger = $logger;
    $this->mimeTypeGuesser = $mimeTypeGuesser;
    $this->eventDispatcher = $event_dispatcher;
    $this->cacheBackend = $file_extractor_cache;

    // Set extraction settings default value. Try to load from global config,
    // complete with default values from ExtractionSettingsFormHelper.
    $config = $this->configFactory->get(SettingsForm::CONFIG_NAME);
    /** @var array $config_extraction_settings */
    $config_extraction_settings = $config->get('extraction_settings') ?? [];
    $this->extractionSettings = $config_extraction_settings + ExtractionSettingsFormHelper::DEFAULT_EXTRACTION_SETTINGS;
  }

  /**
   * {@inheritdoc}
   */
  public function extract(FileInterface $file): string {
    $extracted_data = '';
    if (!$this->isFileIndexable($file)) {
      return $extracted_data;
    }

    // Get cache or extract.
    // Include in the cache ID a hash of the extraction settings able to impact
    // the extraction result.
    $cid = 'extraction:' . $file->id() . ':' . \hash('sha256', \serialize($this->extractionSettings['extraction_result']));
    $cached_data = $this->cacheBackend->get($cid);
    if (!$cached_data) {
      $extracted_data = $this->extractFile($file);

      $tags = \array_merge($file->getCacheTags(), [
        // If the extraction method is changed, cache needs to be invalidated.
        'config:' . SettingsForm::CONFIG_NAME,
      ]);

      $this->cacheBackend->set($cid, $extracted_data, CacheBackendInterface::CACHE_PERMANENT, $tags);
    }
    else {
      /** @var string $extracted_data */
      $extracted_data = $cached_data->data;
    }

    return $extracted_data;
  }

  /**
   * {@inheritdoc}
   */
  public function setExtractionSettings(array $extraction_settings): void {
    $this->extractionSettings = $extraction_settings + $this->extractionSettings;
  }

  /**
   * Helper function.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file entity to extract.
   *
   * @return string
   *   The file extracted content.
   */
  protected function extractFile(FileInterface $file): string {
    $config = $this->configFactory->get(SettingsForm::CONFIG_NAME);
    /** @var string|null $extractor_plugin_id */
    $extractor_plugin_id = $config->get('extraction_method');
    /** @var array|null $extractor_plugin_settings */
    $extractor_plugin_settings = $config->get('extraction_method_settings');

    if ($extractor_plugin_id === NULL) {
      $this->logger->error('No extraction plugin had been configured yet.');
      return '';
    }

    // Do not return if extractor plugin settings is null because a plugin may
    // not have configuration.
    if ($extractor_plugin_settings === NULL) {
      $extractor_plugin_settings = [];
    }

    try {
      /** @var \Drupal\file_extractor\Extractor\ExtractorPluginInterface $extractor_plugin */
      $extractor_plugin = $this->extractorPluginManager->createInstance($extractor_plugin_id, $extractor_plugin_settings);
    }
    catch (PluginException $exception) {
      $this->logger->error('An error occurred when trying to instantiate an Extractor plugin with ID: @plugin_id. Error message was @msg', [
        '@plugin_id' => $extractor_plugin_id,
        '@msg' => $exception->getMessage(),
      ]);
      return '';
    }

    return $this->limitBytes($extractor_plugin->extract($file));
  }

  /**
   * Check if the file is allowed to be indexed.
   *
   * @param \Drupal\file\FileInterface $file
   *   A file object.
   *
   * @return bool
   *   TRUE or FALSE
   */
  protected function isFileIndexable(FileInterface $file): bool {
    $uri = $file->getFileUri();
    if ($uri == NULL) {
      return FALSE;
    }

    // File should exist.
    if (!\file_exists($uri)) {
      return FALSE;
    }

    // File should have a mime type that is allowed.
    $all_excluded_mimes = $this->getMimesForExtensions($this->extractionSettings['extractable']['excluded_extensions']);
    if (\in_array($file->getMimeType(), $all_excluded_mimes, TRUE)) {
      return FALSE;
    }

    // File permanent.
    if (!$file->isPermanent()) {
      return FALSE;
    }

    // File shouldn't exceed configured file size.
    if (!$this->isFileSizeAllowed($file)) {
      return FALSE;
    }

    // Whether a private file can be indexed or not.
    if (!$this->isPrivateFileAllowed($file)) {
      return FALSE;
    }

    // Allow other modules to decide if the file is indexable with an event.
    $event = new FileIndexableEvent($file);
    $this->eventDispatcher->dispatch($event);
    if (!$event->isIndexable()) {
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Get a corresponding array of mime types.
   *
   * Obtained from a space separated string of file extensions.
   *
   * @param string $extensions_string
   *   Extensions string.
   *
   * @return array
   *   Array or mimes.
   */
  protected function getMimesForExtensions(string $extensions_string): array {
    if (!isset($this->mimesForExtensions[$extensions_string])) {
      $extensions = \explode(' ', $extensions_string);
      $mimes = [];
      foreach ($extensions as $extension) {
        $mime = $this->mimeTypeGuesser->guessMimeType('dummy.' . $extension);
        if ($mime !== NULL) {
          $mimes[] = $mime;
        }
      }
      // Ensure we get an array of unique mime values because many extension can
      // map the the same mime type.
      $this->mimesForExtensions[$extensions_string] = \array_unique($mimes);
    }

    return $this->mimesForExtensions[$extensions_string];
  }

  /**
   * Exclude files that exceed configured max size.
   *
   * @param \Drupal\file\FileInterface $file
   *   File object.
   *
   * @return bool
   *   TRUE if the file size does not exceed configured max size.
   */
  protected function isFileSizeAllowed(FileInterface $file): bool {
    $max_file_size = $this->extractionSettings['extractable']['max_filesize'];
    if (!empty($max_file_size) && $max_file_size != '0') {
      $file_size_bytes = $file->getSize();
      $max_size_bytes = Bytes::toNumber($max_file_size);
      if ($file_size_bytes > $max_size_bytes) {
        return FALSE;
      }
    }

    return TRUE;
  }

  /**
   * Exclude private files from being indexed.
   *
   * @param \Drupal\file\FileInterface $file
   *   File object.
   *
   * @return bool
   *   TRUE if we should prevent current file from being indexed.
   */
  protected function isPrivateFileAllowed(FileInterface $file): bool {
    $exclude_private = $this->extractionSettings['extractable']['exclude_private'];

    // Know if private files are allowed to be indexed.
    $private_allowed = !$exclude_private;
    // Know if current file is private.
    $uri = $file->getFileUri();
    if ($uri == NULL) {
      return TRUE;
    }

    $file_is_private = FALSE;
    if (\substr($uri, 0, self::PRIVATE_SCHEME_PREFIX_LENGTH) == 'private://') {
      $file_is_private = TRUE;
    }

    if (!$file_is_private) {
      return TRUE;
    }

    return $private_allowed;
  }

  /**
   * Limit the indexed text to first N bytes.
   *
   * @param string $extracted_text
   *   The whole extracted text.
   *
   * @return string
   *   The first N bytes of the extracted text that will be indexed and cached.
   */
  protected function limitBytes(string $extracted_text): string {
    // Bytes::toInt should be an int, but in practice it is float.
    $bytes = (int) (Bytes::toNumber($this->extractionSettings['extraction_result']['number_first_bytes']));

    // If $bytes is 0 return all items.
    if ($bytes == 0) {
      return $extracted_text;
    }

    return \mb_strcut($extracted_text, 0, $bytes);
  }

}
