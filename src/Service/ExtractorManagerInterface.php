<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Service;

use Drupal\file\FileInterface;

/**
 * Extractor manager interface methods.
 */
interface ExtractorManagerInterface {

  /**
   * Extract file content.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file to extract content from.
   *
   * @return string
   *   The file extracted content.
   */
  public function extract(FileInterface $file): string;

  /**
   * Override extraction settings.
   *
   * @param array $extraction_settings
   *   Extraction settings to override the ones provided by
   *   file_extractor.settings.
   */
  public function setExtractionSettings(array $extraction_settings): void;

}
