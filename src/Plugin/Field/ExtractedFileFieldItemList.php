<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Plugin\Field;

use Drupal\Core\Field\FieldItemList;
use Drupal\Core\TypedData\ComputedItemListTrait;
use Drupal\file\FileInterface;

/**
 * A computed field that provides a file entity's extracted file.
 */
class ExtractedFileFieldItemList extends FieldItemList {

  use ComputedItemListTrait {
    get as traitGet;
  }

  /**
   * {@inheritdoc}
   */
  protected function computeValue(): void {
    $entity = $this->getEntity();

    // Be sure to manipulate a File Entity.
    if (!($entity instanceof FileInterface)) {
      return;
    }

    // There does ot seem to have dependency injection in FieldItemList.
    /** @var \Drupal\file_extractor\Service\ExtractorManagerInterface $extractor_manager */
    $extractor_manager = \Drupal::service('file_extractor.extractor_manager');

    $extracted_data = $extractor_manager->extract($entity);

    // Do not store NULL values, in the case where a file extraction is not
    // done yet or is empty, we do not create list items for the computed field.
    if (!empty($extracted_data)) {
      // An entity can only have a single extracted data.
      /** @var \Drupal\Core\Field\FieldItemInterface $field_item */
      $field_item = $this->createItem(0, $extracted_data);
      $this->list[0] = $field_item;
    }
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore-next-line
   */
  public function get($index) {
    if ($index !== 0) {
      throw new \InvalidArgumentException('A file entity can not have multiple extracted data at the same time.');
    }
    return $this->traitGet($index);
  }

}
