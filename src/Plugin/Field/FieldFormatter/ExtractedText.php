<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Plugin\Field\FieldFormatter;

use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Field\Attribute\FieldFormatter;
use Drupal\Core\Field\FieldItemListInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\Plugin\Field\FieldFormatter\FileFormatterBase;
use Drupal\file_extractor\ExtractionSettingsFormHelper;
use Drupal\file_extractor\Service\ExtractorManagerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * File formatter displaying text extracted form attachment document.
 */
#[FieldFormatter(
  id: 'file_extractor_extracted_text',
  label: new TranslatableMarkup('File Extractor: extracted file'),
  field_types: [
    'file',
  ],
)]
class ExtractedText extends FileFormatterBase implements ContainerFactoryPluginInterface {

  /**
   * The extractor manager.
   *
   * @var \Drupal\file_extractor\Service\ExtractorManagerInterface
   */
  protected ExtractorManagerInterface $extractorManager;

  /**
   * Class resolver service.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected ClassResolverInterface $classResolver;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition): self {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->extractorManager = $container->get('file_extractor.extractor_manager');
    $instance->classResolver = $container->get('class_resolver');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public static function defaultSettings() {
    return [
      'override_global_extraction_settings' => FALSE,
      'extraction_settings' => ExtractionSettingsFormHelper::DEFAULT_EXTRACTION_SETTINGS,
    ] + parent::defaultSettings();
  }

  /**
   * {@inheritdoc}
   */
  public function settingsForm(array $form, FormStateInterface $form_state) {
    $form = parent::settingsForm($form, $form_state);

    $form['override_global_extraction_settings'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Override global extraction settings'),
      '#default_value' => $this->getSetting('override_global_extraction_settings'),
    ];

    $form['extraction_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Extraction settings'),
      '#open' => $this->getSetting('override_global_extraction_settings'),
      '#tree' => TRUE,
      '#states' => [
        'invisible' => [
          ':input[name$="[settings][override_global_extraction_settings]"]' => ['checked' => FALSE],
        ],
      ],
    ];

    /** @var array $extraction_settings */
    $extraction_settings = $this->getSetting('extraction_settings');
    $form_state->setTemporary($extraction_settings);
    $extraction_settings_form_state = SubformState::createForSubform($form['extraction_settings'], $form, $form_state);
    /** @var \Drupal\file_extractor\ExtractionSettingsFormHelper $extraction_settings_form_helper */
    $extraction_settings_form_helper = $this->classResolver->getInstanceFromDefinition(ExtractionSettingsFormHelper::class);
    $form['extraction_settings'] = $extraction_settings_form_helper->buildConfigurationForm($form['extraction_settings'], $extraction_settings_form_state);

    $form['#element_validate'] = [[static::class, 'validateSettingsForm']];
    return $form;
  }

  /**
   * Validate the settings form.
   *
   * @param array $element
   *   The form element to validate.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   * @param array $form
   *   The form representation.
   */
  public static function validateSettingsForm(array $element, FormStateInterface $form_state, array $form): void {
    $extraction_settings_form_state = SubformState::createForSubform($element['extraction_settings'], $form, $form_state);
    /** @var \Drupal\file_extractor\ExtractionSettingsFormHelper $extraction_settings_form_helper */
    $extraction_settings_form_helper = \Drupal::service('class_resolver')->getInstanceFromDefinition(ExtractionSettingsFormHelper::class);
    $extraction_settings_form_helper->validateConfigurationForm($element['extraction_settings'], $extraction_settings_form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function settingsSummary() {
    $summary = [];
    if (!$this->getSetting('override_global_extraction_settings')) {
      $summary[] = $this->t('Global extraction settings not overridden');
    }
    else {
      $summary[] = $this->t('Global extraction settings overridden. Overridden values:');
      /** @var array $extraction_settings */
      $extraction_settings = $this->getSetting('extraction_settings');
      foreach ($extraction_settings as $extraction_settings_category) {
        foreach ($extraction_settings_category as $extraction_setting_key => $extraction_setting_value) {
          $summary[] = $this->t('@extraction_setting_key: @extraction_setting_value', [
            '@extraction_setting_key' => $extraction_setting_key,
            '@extraction_setting_value' => $extraction_setting_value,
          ]);
        }
      }
    }
    return $summary;
  }

  /**
   * {@inheritdoc}
   */
  public function viewElements(FieldItemListInterface $items, $langcode) {
    /** @var \Drupal\Core\Field\EntityReferenceFieldItemListInterface $items */
    $elements = [];

    if ($this->getSetting('override_global_extraction_settings')) {
      /** @var array $extraction_settings */
      $extraction_settings = $this->getSetting('extraction_settings');
      $this->extractorManager->setExtractionSettings($extraction_settings);
    }

    foreach ($this->getEntitiesToView($items, $langcode) as $delta => $file) {
      /** @var \Drupal\file\FileInterface $file */
      $elements[$delta] = [
        '#markup' => $this->extractorManager->extract($file),
        '#cache' => [
          'tags' => $file->getCacheTags(),
        ],
      ];
    }

    return $elements;
  }

}
