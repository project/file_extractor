<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Plugin\file_extractor\Extractor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\FileInterface;
use Drupal\file_extractor\Attribute\FileExtractorExtractor;
use Drupal\file_extractor\Extractor\ExtractorPluginBase;
use GuzzleHttp\ClientInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Provides tika server extractor.
 */
#[FileExtractorExtractor(
  id: 'tika_server_extractor',
  label: new TranslatableMarkup('Tika JAX-RS Server Extractor'),
  description: new TranslatableMarkup('Adds Tika JAX-RS server extractor support.'),
)]
class TikaServerExtractor extends ExtractorPluginBase implements PluginFormInterface {

  /**
   * Tika server default port.
   */
  public const TIKA_SERVER_DEFAULT_PORT = 9998;

  /**
   * Requests to Tika server default timeout.
   */
  public const DEFAULT_TIMEOUT = 5;

  /**
   * Maximum port number.
   */
  public const MAX_PORT = 65535;

  /**
   * The HTTP client.
   *
   * @var \GuzzleHttp\Client
   */
  protected ClientInterface $httpClient;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    $instance = parent::create($container, $configuration, $plugin_id, $plugin_definition);
    $instance->httpClient = $container->get('http_client');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'scheme' => 'http',
      'host' => 'localhost',
      'port' => self::TIKA_SERVER_DEFAULT_PORT,
      'timeout' => self::DEFAULT_TIMEOUT,
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['scheme'] = [
      '#type' => 'select',
      '#title' => $this->t('HTTP protocol'),
      '#description' => $this->t('The HTTP protocol to use for sending queries.'),
      '#default_value' => $this->configuration['scheme'],
      '#required' => TRUE,
      '#options' => [
        'http' => $this->t('http'),
        'https' => $this->t('https'),
      ],
    ];

    $form['host'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Tika server host'),
      '#description' => $this->t('The host name or IP of your Tika server. Example: "localhost" or "www.example.com".'),
      '#default_value' => $this->configuration['host'],
      '#required' => TRUE,
    ];

    $form['port'] = [
      '#type' => 'number',
      '#title' => $this->t('Tika server port'),
      '#description' => $this->t('The default port is @default_port.', [
        '@default_port' => self::TIKA_SERVER_DEFAULT_PORT,
      ]),
      '#default_value' => $this->configuration['port'],
      '#required' => TRUE,
      '#min' => 0,
      '#max' => self::MAX_PORT,
    ];

    $form['timeout'] = [
      '#type' => 'number',
      '#title' => $this->t('Query timeout'),
      '#description' => $this->t('The timeout in seconds for queries sent to the Tika server.'),
      '#default_value' => $this->configuration['timeout'],
      '#required' => TRUE,
      '#min' => 1,
      '#max' => (int) 180,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function extract(FileInterface $file): string {
    $uri = $file->getFileUri();
    if ($uri == NULL) {
      return '';
    }

    $options = [
      'timeout' => $this->configuration['timeout'],
      'body' => \fopen($uri, 'rb'),
      'headers' => [
        'Accept' => 'text/plain',
      ],
    ];

    try {
      $response = $this->httpClient->request('PUT', $this->getServerUri() . '/tika', $options);
      return (string) $response->getBody();
    }
    catch (\Exception $exception) {
      $log_variables = [
        '@file_path' => $this->getRealpath($uri),
        '@msg' => $exception->getMessage(),
      ];
      $this->logger->error('Caught exception trying to import the file %url to %uri. Error message was @msg', $log_variables);
      $this->logger->error('An error occurred during the extraction of the file @file_path with the Tika server. Error message was @msg', $log_variables);
      return '';
    }
  }

  /**
   * Returns the Tika server URI from the current config.
   *
   * @return string
   *   The full Tika server URI.
   */
  protected function getServerUri(): string {
    return $this->configuration['scheme'] . '://' . $this->configuration['host'] . ':' . $this->configuration['port'];
  }

}
