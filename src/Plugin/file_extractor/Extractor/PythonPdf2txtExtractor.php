<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Plugin\file_extractor\Extractor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\FileInterface;
use Drupal\file_extractor\Attribute\FileExtractorExtractor;
use Drupal\file_extractor\Extractor\ExtractorPluginBase;
use Symfony\Component\Process\Process;

/**
 * Provides Python pdf2text extractor.
 */
#[FileExtractorExtractor(
  id: 'python_pdf2txt_extractor',
  label: new TranslatableMarkup('Python Pdf2txt Extractor'),
  description: new TranslatableMarkup('Adds Python Pdf2txt extractor support.'),
  packageDependencies: ['symfony/process'],
)]
class PythonPdf2txtExtractor extends ExtractorPluginBase implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'python_path' => 'python',
      'python_pdf2txt_script' => '/usr/bin/pdf2txt.py',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['python_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Python binary'),
      '#description' => $this->t('Enter the name of @binary executable or the full path to the @binary binary. Example: "@binary_example" or "@full_path_example".', [
        '@binary' => 'python',
        '@binary_example' => 'python3',
        '@full_path_example' => '/usr/bin/python3',
      ]),
      '#default_value' => $this->configuration['python_path'],
      '#required' => TRUE,
    ];
    $form['python_pdf2txt_script'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Python pdf2txt script'),
      '#description' => $this->t('Enter the full path to the Python pdf2txt script. Example: "@example_path".', [
        '@example_path' => '/usr/bin/pdf2txt',
      ]),
      '#default_value' => $this->configuration['python_pdf2txt_script'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $path_keys = [
      'python_path',
      'python_pdf2txt_script',
    ];
    foreach ($path_keys as $path_key) {
      /** @var string $path */
      $path = $form_state->getValue($path_key);
      if (!$this->isBinaryName($path) && !\file_exists($path)) {
        $form_state->setError($form[$path_key], $this->t('The file %path does not exist.', ['%path' => $path]));
      }
    }

    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function extract(FileInterface $file): string {
    // This extractor can only extract PDF.
    if (!\in_array($file->getMimeType(), $this->getPdfMimeTypes(), TRUE)) {
      return '';
    }

    $log_variables = [];
    $python_path = $this->configuration['python_path'];
    $python_pdf2txt_script = \realpath($this->configuration['python_pdf2txt_script']) ?: '';
    $uri = $file->getFileUri();
    if ($uri == NULL) {
      return '';
    }
    $file_path = $this->getRealpath($uri);

    // Check Python binary and Python pdf2txt script.
    if (!$this->checkBinaryAndFile($python_path, $python_pdf2txt_script, $log_variables)) {
      return '';
    }

    // Check file to extract. It will re-check Python binary; but this avoid
    // code duplication.
    if (!$this->checkBinaryAndFile($python_path, $file_path, $log_variables)) {
      return '';
    }

    $process_arguments = [
      $python_path,
      $python_pdf2txt_script,
      '-C',
      '-t',
      'text',
      $file_path,
    ];
    $env_variables = ['LANG' => $this->getUtf8Locale()];
    $extraction_process = new Process($process_arguments, NULL, $env_variables);
    $extraction_process->run();

    if (!$extraction_process->isSuccessful()) {
      $log_variables['@error_message'] = $extraction_process->getErrorOutput();
      $this->logger->error('An error occurred during the extraction of the file @file_path with the binary @binary_path. The error was: @error_message.', $log_variables);
      return '';
    }
    return $extraction_process->getOutput();
  }

}
