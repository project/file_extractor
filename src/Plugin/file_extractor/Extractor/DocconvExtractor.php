<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Plugin\file_extractor\Extractor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\FileInterface;
use Drupal\file_extractor\Attribute\FileExtractorExtractor;
use Drupal\file_extractor\Extractor\ExtractorPluginBase;
use Symfony\Component\Process\Process;

/**
 * Provides docconv extractor.
 */
#[FileExtractorExtractor(
  id: 'docconv_extractor',
  label: new TranslatableMarkup('Docconv Extractor'),
  description: new TranslatableMarkup('Adds Docconv extractor support.'),
  packageDependencies: ['symfony/process'],
)]
class DocconvExtractor extends ExtractorPluginBase implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'docconv_path' => '/usr/bin/docd',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['docconv_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Docconv binary'),
      '#description' => $this->t('Enter the name of @binary executable or the full path to the @binary binary. Example: "@binary_example" or "@full_path_example".', [
        '@binary' => 'docconv',
        '@binary_example' => 'docd',
        '@full_path_example' => '/usr/bin/docd',
      ]),
      '#default_value' => $this->configuration['docconv_path'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    /** @var string $docconv_path */
    $docconv_path = $form_state->getValue('docconv_path');
    if (!$this->isBinaryName($docconv_path) && !\file_exists($docconv_path)) {
      $form_state->setError($form['docconv_path'], $this->t('The file %path does not exist.', ['%path' => $docconv_path]));
    }

    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function extract(FileInterface $file): string {
    $log_variables = [];
    $docconv_path = $this->configuration['docconv_path'];
    $uri = $file->getFileUri();
    if ($uri == NULL) {
      return '';
    }
    $file_path = $this->getRealpath($uri);

    if (!$this->checkBinaryAndFile($docconv_path, $file_path, $log_variables)) {
      return '';
    }

    $process_arguments = [
      $docconv_path,
      '-input',
      $file_path,
    ];
    $env_variables = ['LANG' => $this->getUtf8Locale()];
    $extraction_process = new Process($process_arguments, NULL, $env_variables);
    $extraction_process->run();

    if (!$extraction_process->isSuccessful()) {
      $log_variables['@error_message'] = $extraction_process->getErrorOutput();
      $this->logger->error('An error occurred during the extraction of the file @file_path with the binary @binary_path. The error was: @error_message.', $log_variables);
      return '';
    }
    return $extraction_process->getOutput();
  }

}
