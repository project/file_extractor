<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Plugin\file_extractor\Extractor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\FileInterface;
use Drupal\file_extractor\Attribute\FileExtractorExtractor;
use Drupal\file_extractor\Extractor\ExtractorPluginBase;
use Symfony\Component\Process\Process;

/**
 * Provides pdftotext extractor.
 */
#[FileExtractorExtractor(
  id: 'pdftotext_extractor',
  label: new TranslatableMarkup('Pdftotext Extractor'),
  description: new TranslatableMarkup('Adds Pdftotext extractor support.'),
  packageDependencies: ['symfony/process'],
)]
class PdftotextExtractor extends ExtractorPluginBase implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'pdftotext_path' => 'pdftotext',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['pdftotext_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Pdftotext binary'),
      '#description' => $this->t('Enter the name of @binary executable or the full path to the @binary binary. Example: "@binary_example" or "@full_path_example".', [
        '@binary' => 'pdftotext',
        '@binary_example' => 'pdftotext',
        '@full_path_example' => '/usr/bin/pdftotext',
      ]),
      '#default_value' => $this->configuration['pdftotext_path'],
      '#required' => TRUE,
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    /** @var string $pdftotext_path */
    $pdftotext_path = $form_state->getValue('pdftotext_path');
    if (!$this->isBinaryName($pdftotext_path) && !\file_exists($pdftotext_path)) {
      $form_state->setError($form['pdftotext_path'], $this->t('The file %path does not exist.', ['%path' => $pdftotext_path]));
    }

    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function extract(FileInterface $file): string {
    // This extractor can only extract PDF.
    if (!\in_array($file->getMimeType(), $this->getPdfMimeTypes(), TRUE)) {
      return '';
    }

    $log_variables = [];
    $pdftotext_path = $this->configuration['pdftotext_path'];
    $uri = $file->getFileUri();
    if ($uri == NULL) {
      return '';
    }
    $file_path = $this->getRealpath($uri);

    if (!$this->checkBinaryAndFile($pdftotext_path, $file_path, $log_variables)) {
      return '';
    }

    $process_arguments = [
      $pdftotext_path,
      $file_path,
      // Pdftotext description states that '-' as text-file will send text to
      // stdout.
      '-',
    ];
    $env_variables = ['LANG' => $this->getUtf8Locale()];
    $extraction_process = new Process($process_arguments, NULL, $env_variables);
    $extraction_process->run();

    if (!$extraction_process->isSuccessful()) {
      $log_variables['@error_message'] = $extraction_process->getErrorOutput();
      $this->logger->error('An error occurred during the extraction of the file @file_path with the binary @binary_path. The error was: @error_message.', $log_variables);
      return '';
    }
    return $extraction_process->getOutput();
  }

}
