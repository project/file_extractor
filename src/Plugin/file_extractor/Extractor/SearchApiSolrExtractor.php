<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Plugin\file_extractor\Extractor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\FileInterface;
use Drupal\file_extractor\Attribute\FileExtractorExtractor;
use Drupal\file_extractor\Extractor\ExtractorPluginBase;
use Drupal\search_api\SearchApiException;
use Drupal\search_api_solr\SolrBackendInterface;

/**
 * Provides Search API Solr extractor.
 */
#[FileExtractorExtractor(
  id: 'search_api_solr_extractor',
  label: new TranslatableMarkup('Search API Solr Extractor'),
  description: new TranslatableMarkup('Adds Search API Solr extractor support.'),
  packageDependencies: ['search_api_solr'],
)]
class SearchApiSolrExtractor extends ExtractorPluginBase implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'solr_server' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form = [];
    $conditions = [
      'status' => TRUE,
    ];

    /** @var \Drupal\search_api\ServerInterface[] $search_api_solr_servers */
    $search_api_solr_servers = $this->entityTypeManager->getStorage('search_api_server')->loadByProperties($conditions);
    $options = [];
    foreach ($search_api_solr_servers as $solr_server) {
      if ($solr_server->hasValidBackend() && $solr_server->getBackend() instanceof SolrBackendInterface) {
        $options[$solr_server->id()] = $solr_server->label();
      }
    }

    $form['solr_server'] = [
      '#type' => 'select',
      '#title' => $this->t('Solr server'),
      '#description' => $this->t('Select the Search API Solr server you want to use.'),
      '#default_value' => $this->configuration['solr_server'],
      '#required' => TRUE,
      '#options' => $options,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function extract(FileInterface $file): string {
    $solr_server_id = $this->configuration['solr_server'];
    $uri = $file->getFileUri();
    if ($uri == NULL) {
      return '';
    }
    $file_path = $this->getRealpath($uri);

    $log_variables = [
      '@file_path' => $file_path,
      '@solr_server_id' => $solr_server_id,
    ];

    // Load the chosen Solr server entity.
    $conditions = [
      'status' => TRUE,
      'id' => $solr_server_id,
    ];
    /** @var \Drupal\search_api\ServerInterface[] $servers */
    $servers = $this->entityTypeManager->getStorage('search_api_server')->loadByProperties($conditions);

    // Check that the config still exists.
    if (empty($servers)) {
      $this->logger->error('Impossible to load the configuration of the Solr server @solr_server_id.', $log_variables);
      return '';
    }
    $server = \reset($servers);

    // Check that the backend plugin still exists.
    try {
      /** @var \Drupal\search_api_solr\SolrBackendInterface $solr_backend */
      $solr_backend = $server->getBackend();
    }
    catch (SearchApiException $exception) {
      $log_variables['@msg'] = $exception->getMessage();
      $this->logger->error('Impossible to load the backend plugin of the Solr server @solr_server_id. Error message was @msg', $log_variables);
      return '';
    }

    // Check that the backend is available.
    if (!$solr_backend->isAvailable()) {
      $this->logger->error('The Solr server @solr_server_id is not available.', $log_variables);
      return '';
    }

    // Extract the content.
    try {
      $xml_data = $solr_backend->extractContentFromFile($file_path);
    }
    catch (SearchApiException $exception) {
      $log_variables['@msg'] = $exception->getMessage();
      $this->logger->error('An error occurred during the extraction of the file @file_path with the Solr server @solr_server_id. Error message was @msg', $log_variables);
      return '';
    }

    return $this->extractBody($xml_data);
  }

  /**
   * Extract the body from XML response.
   *
   * @param string $xml_data
   *   The XML data from the Solr server.
   *
   * @return string
   *   The parsed data.
   */
  protected function extractBody(string $xml_data): string {
    $matches = [];
    if (!\preg_match(',<body[^>]*>(.*)</body>,sim', $xml_data, $matches)) {
      // If the body can't be found return just the text. This will be safe
      // and contain any text to index.
      return \strip_tags($xml_data);
    }
    // Return the full content of the body. Including tags that can optionally
    // be used for index weight.
    return $matches[1];
  }

}
