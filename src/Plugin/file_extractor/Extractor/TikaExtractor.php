<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Plugin\file_extractor\Extractor;

use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\Core\StringTranslation\TranslatableMarkup;
use Drupal\file\FileInterface;
use Drupal\file_extractor\Attribute\FileExtractorExtractor;
use Drupal\file_extractor\Extractor\ExtractorPluginBase;
use Symfony\Component\Process\Process;

/**
 * Provides tika extractor.
 */
#[FileExtractorExtractor(
  id: 'tika_extractor',
  label: new TranslatableMarkup('Tika Extractor'),
  description: new TranslatableMarkup('Adds Tika extractor support.'),
  packageDependencies: ['symfony/process'],
)]
class TikaExtractor extends ExtractorPluginBase implements PluginFormInterface {

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [
      'java_path' => 'java',
      'tika_path' => '',
      'tika_config_path' => '',
    ] + parent::defaultConfiguration();
  }

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state) {
    $form['java_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Java binary'),
      '#description' => $this->t('Enter the name of @binary executable or the full path to the @binary binary. Example: "@binary_example" or "@full_path_example".', [
        '@binary' => 'java',
        '@binary_example' => 'java',
        '@full_path_example' => '/usr/bin/java',
      ]),
      '#default_value' => $this->configuration['java_path'],
      '#required' => TRUE,
    ];
    $form['tika_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to Tika .jar file'),
      '#description' => $this->t('Enter the full path to the Tika executable jar file. Example: "@example_path".', [
        '@example_path' => '/var/apache-tika/tika-app-1.28.5.jar',
      ]),
      '#default_value' => $this->configuration['tika_path'],
      '#required' => TRUE,
    ];
    $form['tika_config_path'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Path to Tika config file'),
      '#description' => $this->t('Enter the full path to the Tika config file. Example: "@example_path".', [
        '@example_path' => '/var/apache-tika/tika-config.xml',
      ]),
      '#default_value' => $this->configuration['tika_config_path'],
    ];
    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $java_path = $form_state->getValue('java_path');
    /** @var string $tika_path */
    $tika_path = $form_state->getValue('tika_path');
    /** @var string $tika_config_path */
    $tika_config_path = $form_state->getValue('tika_config_path');

    // Check Java path.
    $java_process = new Process([$java_path]);
    $java_process->run();
    // Exit code equals 127 if it fails. 1 instead.
    if ($java_process->getExitCode() != 1) {
      $form_state->setError($form['java_path'], $this->t('Invalid path or filename %path for Java binary.', ['%path' => $java_path]));
      return;
    }

    // Check Tika path.
    if (!\file_exists($tika_path)) {
      $form_state->setError($form['tika_path'], $this->t('The file %path does not exist.', ['%path' => $tika_path]));
    }
    // Check return code.
    else {
      $tika_process = new Process([
        $java_path,
        '-jar',
        $tika_path,
        '-V',
      ]);
      $tika_process->run();

      // Exit code equals 1 if it fails. 0 instead.
      if ($tika_process->getExitCode()) {
        $form_state->setError($form['tika_path'], $this->t('Tika could not be reached and executed.'));
      }
    }

    // Check Tika config path.
    if (!empty($tika_config_path) && !\file_exists($tika_config_path)) {
      $form_state->setError($form['tika_config_path'], $this->t('The file %path does not exist.', ['%path' => $tika_config_path]));
    }

    parent::validateConfigurationForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function extract(FileInterface $file): string {
    $log_variables = [];
    $java_path = $this->configuration['java_path'];
    $tika_path = \realpath($this->configuration['tika_path']) ?: '';
    $tika_config_path = '';
    if (!empty($this->configuration['tika_config_path'])) {
      $tika_config_path = \realpath($this->configuration['tika_config_path']);
    }
    $uri = $file->getFileUri();
    if ($uri == NULL) {
      return '';
    }
    $file_path = $this->getRealpath($uri);

    // Check Java binary and Tika file.
    if (!$this->checkBinaryAndFile($java_path, $tika_path, $log_variables)) {
      return '';
    }

    // Check file to extract. It will re-check Java binary; but this avoid
    // code duplication.
    if (!$this->checkBinaryAndFile($java_path, $file_path, $log_variables)) {
      return '';
    }

    $process_arguments = [];
    $env_variables = ['LANG' => $this->getUtf8Locale()];

    $extension_dir = \ini_get('extension_dir') ?: '';
    if (\strpos($extension_dir, 'MAMP/')) {
      $env_variables['DYLD_LIBRARY_PATH'] = '""';
    }

    $process_arguments[] = $java_path;

    // Force running the Tika jar headless.
    $process_arguments[] = '-Djava.awt.headless=true';
    if ($file->getMimeType() != 'audio/mpeg') {
      $process_arguments[] = '-Dfile.encoding=UTF8';
      $process_arguments[] = '-cp';
      $process_arguments[] = $tika_path;
    }
    $process_arguments[] = '-jar';
    $process_arguments[] = $tika_path;
    if (!empty($tika_config_path)) {
      $process_arguments[] = '--config=' . $tika_config_path;
    }
    $process_arguments[] = '-t';
    $process_arguments[] = $file_path;

    $extraction_process = new Process($process_arguments, NULL, $env_variables);
    $extraction_process->run();

    if (!$extraction_process->isSuccessful()) {
      $log_variables['@error_message'] = $extraction_process->getErrorOutput();
      $this->logger->error('An error occurred during the extraction of the file @file_path with the binary @binary_path. The error was: @error_message.', $log_variables);
      return '';
    }
    return $extraction_process->getOutput();
  }

}
