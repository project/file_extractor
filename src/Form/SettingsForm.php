<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Form;

use Drupal\Component\Plugin\Exception\PluginException;
use Drupal\Component\Utility\Html;
use Drupal\Core\DependencyInjection\ClassResolverInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformState;
use Drupal\Core\Plugin\PluginFormInterface;
use Drupal\file_extractor\ExtractionSettingsFormHelper;
use Drupal\file_extractor\Extractor\ExtractorPluginManager;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configuration form.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * Name of the config being edited.
   */
  public const CONFIG_NAME = 'file_extractor.settings';

  /**
   * Text extractor plugin Manager.
   *
   * @var \Drupal\file_extractor\Extractor\ExtractorPluginManager
   */
  protected ExtractorPluginManager $extractorPluginManager;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * Class resolver service.
   *
   * @var \Drupal\Core\DependencyInjection\ClassResolverInterface
   */
  protected ClassResolverInterface $classResolver;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    $instance = parent::create($container);
    $instance->extractorPluginManager = $container->get('plugin.manager.file_extractor.extractor');
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->classResolver = $container->get('class_resolver');
    return $instance;
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return [static::CONFIG_NAME];
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'file_extractor_settings';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config(static::CONFIG_NAME);
    $form['extraction_method'] = [
      '#type' => 'select',
      '#title' => $this->t('Extraction method'),
      '#description' => $this->t('Select the extraction method you want to use.'),
      '#options' => $this->getExtractionPluginInformation()['labels'],
      '#default_value' => $config->get('extraction_method'),
      '#required' => TRUE,
      '#ajax' => [
        'callback' => [static::class, 'buildAjaxExtractorConfigForm'],
        'wrapper' => 'file-extractor-extractor-config-form',
        'method' => 'replaceWith',
        'effect' => 'fade',
      ],
    ];

    $this->buildExtractorConfigForm($form, $form_state);

    $form['extraction_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Extraction settings'),
      '#description' => $this->t('Global extraction settings that will be applied by default. Those settings can be overridden programmatically if calling ExtractorManager::extract() directly.'),
      '#open' => TRUE,
      '#tree' => TRUE,
    ];
    /** @var array $config_extraction_settings */
    $config_extraction_settings = $config->get('extraction_settings') ?? [];
    $form_state->setTemporary($config_extraction_settings);
    $extraction_settings_form_state = SubformState::createForSubform($form['extraction_settings'], $form, $form_state);
    /** @var \Drupal\file_extractor\ExtractionSettingsFormHelper $extraction_settings_form_helper */
    $extraction_settings_form_helper = $this->classResolver->getInstanceFromDefinition(ExtractionSettingsFormHelper::class);
    $form['extraction_settings'] = $extraction_settings_form_helper->buildConfigurationForm($form['extraction_settings'], $extraction_settings_form_state);

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state): void {
    // We are in the case of an AJAX trigger. So we do not validate the
    // extractor plugin form.
    if (!$form_state->isSubmitted()) {
      return;
    }

    // Extractor plugin validation.
    /** @var string $selected_extraction_method */
    $selected_extraction_method = $form_state->getValue('extraction_method');
    /** @var array $selected_extraction_method_settings */
    $selected_extraction_method_settings = $form_state->getValue('extraction_method_settings', []);
    try {
      $extractor_plugin = $this->getExtractorPluginManager()->createInstance($selected_extraction_method, $selected_extraction_method_settings);

      if ($extractor_plugin instanceof PluginFormInterface) {
        $extractor_form_state = SubformState::createForSubform($form['extraction_method_settings'], $form, $form_state);
        $extractor_plugin->validateConfigurationForm($form['extraction_method_settings'], $extractor_form_state);
      }
    }
    catch (PluginException $exception) {
      $this->messenger()->addError($this->t('An error occurred when trying to instantiate an Extractor plugin with ID: @plugin_id. Error message was @msg', [
        '@plugin_id' => $selected_extraction_method,
        '@msg' => $exception->getMessage(),
      ]));
    }

    // Extraction settings validation.
    $extraction_settings_form_state = SubformState::createForSubform($form['extraction_settings'], $form, $form_state);
    /** @var \Drupal\file_extractor\ExtractionSettingsFormHelper $extraction_settings_form_helper */
    $extraction_settings_form_helper = $this->classResolver->getInstanceFromDefinition(ExtractionSettingsFormHelper::class);
    $extraction_settings_form_helper->validateConfigurationForm($form['extraction_settings'], $extraction_settings_form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    /** @var string $selected_extraction_method */
    $selected_extraction_method = $form_state->getValue('extraction_method');
    /** @var array $selected_extraction_method_settings */
    $selected_extraction_method_settings = $form_state->getValue('extraction_method_settings', []);

    try {
      /** @var \Drupal\file_extractor\Extractor\ExtractorPluginInterface $extractor_plugin */
      $extractor_plugin = $this->getExtractorPluginManager()->createInstance($selected_extraction_method, $selected_extraction_method_settings);

      if ($extractor_plugin instanceof PluginFormInterface) {
        $extractor_form_state = SubformState::createForSubform($form['extraction_method_settings'], $form, $form_state);
        $extractor_plugin->submitConfigurationForm($form['extraction_method_settings'], $extractor_form_state);
        $selected_extraction_method_settings = $extractor_plugin->getConfiguration();
      }
    }
    catch (PluginException $exception) {
      $this->messenger()->addError($this->t('An error occurred when trying to instantiate an Extractor plugin with ID: @plugin_id. Error message was @msg', [
        '@plugin_id' => $selected_extraction_method,
        '@msg' => $exception->getMessage(),
      ]));
    }

    $this->config(static::CONFIG_NAME)
      ->set('extraction_method', $selected_extraction_method)
      ->set('extraction_method_settings', $selected_extraction_method_settings)
      ->set('extraction_settings', $form_state->getValue('extraction_settings'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Get definition of Extraction plugins from their annotation definition.
   *
   * @return array
   *   Array with 'labels' and 'descriptions' as keys containing plugin ids
   *   and their labels or descriptions.
   */
  protected function getExtractionPluginInformation(): array {
    $options = [
      'labels' => [],
      'descriptions' => [],
    ];
    /** @var array $plugin_definition */
    foreach ($this->getExtractorPluginManager()->getDefinitions() as $plugin_id => $plugin_definition) {
      $options['labels'][$plugin_id] = Html::escape($plugin_definition['label']);
      $options['descriptions'][$plugin_id] = Html::escape($plugin_definition['description']);
    }

    \asort($options['labels']);
    \asort($options['descriptions']);

    return $options;
  }

  /**
   * Subform.
   *
   * It will be updated with Ajax to display the configuration of an
   * extraction plugin method.
   *
   * @param array $form
   *   The form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The form state object.
   */
  public function buildExtractorConfigForm(array &$form, FormStateInterface $form_state): void {
    $form['extraction_method_settings'] = [
      '#type' => 'container',
      '#attributes' => [
        'id' => 'file-extractor-extractor-config-form',
      ],
      '#tree' => TRUE,
    ];

    // If no extraction method is selected yet. This is NULL.
    /** @var string|null $selected_extraction_method */
    $selected_extraction_method = $form_state->getValue('extraction_method');
    /** @var array|null $selected_extraction_method_settings */
    $selected_extraction_method_settings = $form_state->getValue('extraction_method_settings');
    $triggering_element = $form_state->getTriggeringElement();
    // We are NOT in the case of an AJAX trigger. So we load the extraction
    // method from config.
    if (!\is_array($triggering_element)) {
      // If the config does not exist yet, this is NULL.
      /** @var string|null $selected_extraction_method */
      $selected_extraction_method = $this->config(static::CONFIG_NAME)->get('extraction_method');
      /** @var array|null $selected_extraction_method_settings */
      $selected_extraction_method_settings = $this->config(static::CONFIG_NAME)->get('extraction_method_settings');
    }

    if ($selected_extraction_method === NULL) {
      return;
    }
    if ($selected_extraction_method_settings === NULL) {
      $selected_extraction_method_settings = [];
    }

    try {
      $extractor_plugin = $this->getExtractorPluginManager()->createInstance($selected_extraction_method, $selected_extraction_method_settings);

      if ($extractor_plugin instanceof PluginFormInterface) {
        // Attach the extractor plugin configuration form.
        $extractor_form_state = SubformState::createForSubform($form['extraction_method_settings'], $form, $form_state);
        $form['extraction_method_settings'] = $extractor_plugin->buildConfigurationForm($form['extraction_method_settings'], $extractor_form_state);

        // Modify the extractor plugin configuration container element.
        $form['extraction_method_settings']['#type'] = 'details';
        $form['extraction_method_settings']['#title'] = $this->t('Configure %plugin', ['%plugin' => $this->getExtractionPluginInformation()['labels'][$selected_extraction_method]]);
        $form['extraction_method_settings']['#attributes']['id'] = 'file-extractor-extractor-config-form';
        $form['extraction_method_settings']['#open'] = TRUE;
        $form['extraction_method_settings']['#tree'] = TRUE;
      }
    }
    catch (PluginException $exception) {
      $this->messenger()->addError($this->t('An error occurred when trying to instantiate an Extractor plugin with ID: @plugin_id. Error message was @msg', [
        '@plugin_id' => $selected_extraction_method,
        '@msg' => $exception->getMessage(),
      ]));
    }
  }

  /**
   * Ajax callback.
   *
   * @param array $form
   *   The current form array.
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current form state.
   *
   * @return array
   *   The part of the form to return as AJAX.
   */
  public static function buildAjaxExtractorConfigForm(array $form, FormStateInterface $form_state) {
    // We just need to return the relevant part of the form here.
    return $form['extraction_method_settings'];
  }

  /**
   * Returns the extractor plugin manager.
   *
   * This method is required because sometimes with AJAX reloading the attribute
   * 'extractorPluginManager' is NULL.
   *
   * @return \Drupal\file_extractor\Extractor\ExtractorPluginManager
   *   The extractor plugin manager.
   */
  protected function getExtractorPluginManager() {
    // phpcs:disable DrupalPractice.Objects.GlobalDrupal.GlobalDrupal
    // @phpstan-ignore-next-line
    return $this->extractorPluginManager ?: \Drupal::service('plugin.manager.file_extractor.extractor');
    // phpcs:enable DrupalPractice.Objects.GlobalDrupal.GlobalDrupal
  }

}
