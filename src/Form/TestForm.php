<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Form;

use Drupal\Core\Form\FormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Url;
use Drupal\file_extractor\Service\ExtractorManagerInterface;
use Drupal\file_extractor\Service\TestFileHelperInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Test the configuration with potential configuration overrides applied.
 */
class TestForm extends FormBase {

  /**
   * The extractor manager.
   *
   * @var \Drupal\file_extractor\Service\ExtractorManagerInterface
   */
  protected ExtractorManagerInterface $extractorManager;

  /**
   * Test file helper.
   *
   * @var \Drupal\file_extractor\Service\TestFileHelperInterface
   */
  protected TestFileHelperInterface $testFileHelper;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    ExtractorManagerInterface $extractor_manager,
    TestFileHelperInterface $test_file_helper,
  ) {
    $this->extractorManager = $extractor_manager;
    $this->testFileHelper = $test_file_helper;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container): self {
    return new self(
      $container->get('file_extractor.extractor_manager'),
      $container->get('file_extractor.test_file_helper')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'file_extractor_test';
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['description'] = [
      '#type' => 'html_tag',
      '#tag' => 'p',
      '#value' => $this->t('Use this form to test the extraction method configuration while taking potential configuration overrides in the settings.php file into account.'),
    ];

    $form['actions']['#type'] = 'actions';
    $form['actions']['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Test extraction'),
      '#button_type' => 'primary',
    ];

    $config = $this->configFactory()->get(SettingsForm::CONFIG_NAME);
    if ($config->get('extraction_method') === NULL) {
      $this->messenger()->addError($this->t('<a href=":url">Configure File Extractor</a> before testing extraction.', [
        ':url' => Url::fromRoute('file_extractor.settings_form')->toString(),
      ]));

      $form['actions']['submit']['#disabled'] = TRUE;
    }

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    // Ensure extraction settings will not exclude the test file.
    $this->extractorManager->setExtractionSettings([
      'extractable' => [
        'excluded_extensions' => '',
        'max_filesize' => '0',
        'exclude_private' => FALSE,
      ],
      'extraction_result' => [
        'number_first_bytes' => '1 MB',
      ],
    ]);
    $extracted_data = $this->extractorManager->extract($this->testFileHelper->getTestFile());
    $this->testFileHelper->deleteTestFile();

    if (empty($extracted_data)) {
      $this->messenger()->addWarning($this->t('The extraction does not seem to work with this configuration! The attempt to extract a test PDF file returned an empty value.'));
    }
    else {
      $this->messenger()->addStatus($this->t('The extraction is working with this configuration! The attempt to extract a test PDF file returned: @extracted_data.', [
        '@extracted_data' => $extracted_data,
      ]));
    }
  }

}
