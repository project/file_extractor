<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Event;

use Drupal\Component\EventDispatcher\Event;
use Drupal\file\FileInterface;

/**
 * Defines an event to decide if a file entity is indexable.
 */
class FileIndexableEvent extends Event {

  /**
   * The file entity.
   *
   * @var \Drupal\file\FileInterface
   */
  protected FileInterface $file;

  /**
   * If the file is indexable.
   *
   * @var bool
   */
  protected bool $indexable = TRUE;

  /**
   * FileIndexableEvent constructor.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file being manipulated.
   */
  public function __construct(FileInterface $file) {
    $this->file = $file;
  }

  /**
   * Returns the file entity.
   *
   * @return \Drupal\file\FileInterface
   *   The entity being manipulated.
   */
  public function getFile() {
    return $this->file;
  }

  /**
   * If the file is indexable or not.
   *
   * @return bool
   *   If the file is indexable or not.
   */
  public function isIndexable() {
    return $this->indexable;
  }

  /**
   * Indicate if the file is indexable or not.
   *
   * @param bool $indexable
   *   If the file is indexable or not.
   */
  public function setIndexable(bool $indexable): void {
    $this->indexable = $indexable;
  }

}
