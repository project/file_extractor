<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Extractor;

use Composer\InstalledVersions;
use Drupal\Core\Cache\CacheBackendInterface;
use Drupal\Core\Extension\ModuleHandlerInterface;
use Drupal\Core\Plugin\DefaultPluginManager;

/**
 * Manages FileExtractorExtractor plugins.
 */
class ExtractorPluginManager extends DefaultPluginManager {

  /**
   * {@inheritdoc}
   */
  public function __construct(\Traversable $namespaces, CacheBackendInterface $cache_backend, ModuleHandlerInterface $module_handler) {
    $subdir = 'Plugin/file_extractor/Extractor';
    $plugin_interface = 'Drupal\file_extractor\Extractor\ExtractorPluginInterface';
    $plugin_definition_annotation_name = 'Drupal\file_extractor\Annotation\FileExtractorExtractor';
    $plugin_definition_attribute_name = 'Drupal\file_extractor\Attribute\FileExtractorExtractor';
    parent::__construct($subdir, $namespaces, $module_handler, $plugin_interface, $plugin_definition_attribute_name, $plugin_definition_annotation_name);
    $this->alterInfo('file_extractor_extractor_info');
    $this->setCacheBackend($cache_backend, 'file_extractor_extractor_plugins');
  }

  /**
   * {@inheritdoc}
   *
   * @phpstan-ignore-next-line
   */
  protected function alterDefinitions(&$definitions) {
    // Loop through all definitions.
    foreach ($definitions as $definition_key => $definition_info) {
      // Check to see if dependencies key is set.
      if (!empty($definition_info['dependencies'])) {
        // Loop through dependencies to confirm if enabled.
        foreach ($definition_info['dependencies'] as $dependency) {
          // If dependency is not enabled removed from list of definitions.
          if (!$this->moduleHandler->moduleExists($dependency)) {
            unset($definitions[$definition_key]);
            continue;
          }
        }
      }

      // Check Composer packages dependencies.
      if (!empty($definition_info['packageDependencies'])) {
        foreach ($definition_info['packageDependencies'] as $package_dependency) {
          if (!InstalledVersions::isInstalled($package_dependency)) {
            unset($definitions[$definition_key]);
            continue;
          }
        }
      }
    }

    parent::alterDefinitions($definitions);
  }

  /**
   * {@inheritdoc}
   */
  public function createInstance($plugin_id, array $configuration = []) {
    /** @var \Drupal\file_extractor\Extractor\ExtractorPluginInterface $instance */
    $instance = parent::createInstance($plugin_id);
    $instance->setConfiguration($configuration);
    return $instance;
  }

}
