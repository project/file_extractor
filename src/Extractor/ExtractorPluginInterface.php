<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Extractor;

use Drupal\Component\Plugin\ConfigurableInterface;
use Drupal\file\FileInterface;

/**
 * An interface for FileExtractorExtractor plugins.
 */
interface ExtractorPluginInterface extends ConfigurableInterface {

  /**
   * Extract file content.
   *
   * @param \Drupal\file\FileInterface $file
   *   The file object.
   *
   * @return string
   *   The file extracted content. Empty string if an error occurred.
   */
  public function extract(FileInterface $file): string;

}
