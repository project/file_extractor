<?php

declare(strict_types=1);

namespace Drupal\file_extractor\Extractor;

use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Messenger\MessengerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Plugin\PluginBase;
use Drupal\Core\StreamWrapper\StreamWrapperInterface;
use Drupal\Core\StreamWrapper\StreamWrapperManagerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\Mime\MimeTypeGuesserInterface;

/**
 * Base class for FileExtractorExtractor plugins.
 *
 * @ingroup plugin_api
 */
abstract class ExtractorPluginBase extends PluginBase implements ContainerFactoryPluginInterface, ExtractorPluginInterface {

  /**
   * Stream wrapper manager service.
   *
   * @var \Drupal\Core\StreamWrapper\StreamWrapperManagerInterface
   */
  protected StreamWrapperManagerInterface $streamWrapperManager;

  /**
   * Mime type guesser service.
   *
   * @var \Symfony\Component\Mime\MimeTypeGuesserInterface
   */
  protected MimeTypeGuesserInterface $mimeTypeGuesser;

  /**
   * Logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected LoggerInterface $logger;

  /**
   * Entity type manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected EntityTypeManagerInterface $entityTypeManager;

  /**
   * {@inheritdoc}
   */
  public function __construct(
    array $configuration,
    $plugin_id,
    array $plugin_definition,
    StreamWrapperManagerInterface $stream_wrapper_manager,
    MimeTypeGuesserInterface $mime_type_guesser,
    LoggerInterface $logger,
    EntityTypeManagerInterface $entity_type_manager,
    MessengerInterface $messenger,
  ) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->streamWrapperManager = $stream_wrapper_manager;
    $this->mimeTypeGuesser = $mime_type_guesser;
    $this->logger = $logger;
    $this->entityTypeManager = $entity_type_manager;
    $this->messenger = $messenger;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('stream_wrapper_manager'),
      $container->get('file.mime_type.guesser'),
      $container->get('logger.channel.file_extractor'),
      $container->get('entity_type.manager'),
      $container->get('messenger')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function defaultConfiguration() {
    return [];
  }

  /**
   * {@inheritdoc}
   */
  public function getConfiguration() {
    return $this->configuration;
  }

  /**
   * {@inheritdoc}
   */
  public function setConfiguration(array $configuration): void {
    $this->configuration = $configuration + $this->defaultConfiguration();
  }

  /**
   * Form validation handler.
   *
   * @param array $form
   *   An associative array containing the structure of the plugin form as built
   *   by static::buildConfigurationForm().
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {}

  /**
   * Form submission handler.
   *
   * @param array $form
   *   An associative array containing the structure of the plugin form as built
   *   by static::buildConfigurationForm().
   * @param \Drupal\Core\Form\FormStateInterface $form_state
   *   The current state of the complete form.
   */
  public function submitConfigurationForm(array &$form, FormStateInterface $form_state): void {
    $this->setConfiguration($form_state->getValues());
  }

  /**
   * Helper method to get the real path from an uri.
   *
   * @param string $uri
   *   The URI of the file, e.g. public://directory/file.jpg.
   *
   * @return string
   *   The real path to the file if it is a local file. A URL otherwise.
   */
  protected function getRealpath(string $uri): string {
    /** @var \Drupal\Core\StreamWrapper\StreamWrapperInterface $wrapper */
    $wrapper = $this->streamWrapperManager->getViaUri($uri);
    $scheme = $this->streamWrapperManager->getScheme($uri);
    $local_wrappers = $this->streamWrapperManager->getWrappers(StreamWrapperInterface::LOCAL);
    if (\in_array($scheme, \array_keys($local_wrappers), TRUE)) {
      return $wrapper->realpath();
    }

    return $wrapper->getExternalUrl();
  }

  /**
   * Helper method to get the PDF MIME types.
   *
   * @return array
   *   An array of the PDF MIME types.
   */
  protected function getPdfMimeTypes(): array {
    $pdf_mime_types = [];
    $pdf_mime_types[] = $this->mimeTypeGuesser->guessMimeType('dummy.pdf');
    return $pdf_mime_types;
  }

  /**
   * Check if the binary path is a path or only the binary name.
   *
   * @param string $binary_path
   *   The binary path.
   *
   * @return bool
   *   TRUE if only a binary name. FALSE otherwise.
   */
  protected function isBinaryName(string $binary_path): bool {
    if ((\strpos($binary_path, '/') === FALSE) && (\strpos($binary_path, '\\') === FALSE)) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Helper function to check that a binary and a file still exists.
   *
   * @param string $binary_path
   *   The binary path.
   * @param string $file_path
   *   The path to the file to extract.
   * @param array $log_variables
   *   Log variables.
   *
   * @return bool
   *   TRUE if binary and file exist. FALSE otherwise.
   */
  protected function checkBinaryAndFile(string $binary_path, string $file_path, array &$log_variables): bool {
    $log_variables['@binary_path'] = $binary_path;
    $log_variables['@file_path'] = $file_path;

    // Test that the binary is still available.
    if (!$this->isBinaryName($binary_path) && !\file_exists($binary_path)) {
      $this->logger->error('Extractor binary was not found at @binary_path.', $log_variables);
      return FALSE;
    }

    // Test that the file is still available.
    if (!\file_exists($file_path)) {
      $this->logger->error('File was not found at @file_path.', $log_variables);
      return FALSE;
    }

    return TRUE;
  }

  /**
   * Determine suitable language for processing files.
   *
   * @return string
   *   UTF-8 locale, or default if nothing suitable.
   */
  protected function getUtf8Locale(): string {
    /** @var string $original_locale */
    // @phpstan-ignore-next-line
    $original_locale = \setlocale(\LC_CTYPE, 0);
    $preferred_locales = [
      'en_US.UTF-8',
      'C.UTF-8',
    ];

    // Attempt to set the locale to determine what is available.
    $new_locale = \setlocale(\LC_CTYPE, $preferred_locales);
    if ($new_locale) {
      // Restore the locale.
      // @phpstan-ignore-next-line
      \setlocale(\LC_CTYPE, $original_locale);
      return $new_locale;
    }
    return $original_locale;
  }

}
