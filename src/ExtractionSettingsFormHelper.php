<?php

declare(strict_types=1);

namespace Drupal\file_extractor;

use Drupal\Component\Utility\Bytes;
use Drupal\Core\Form\FormStateInterface;
use Drupal\Core\Form\SubformStateInterface;
use Drupal\Core\StringTranslation\StringTranslationTrait;

/**
 * Provides helper methods to validate settings form.
 */
class ExtractionSettingsFormHelper {

  use StringTranslationTrait;

  /**
   * The default extraction settings.
   */
  public const DEFAULT_EXTRACTION_SETTINGS = [
    'extractable' => [
      'excluded_extensions' => 'aif art avi bmp gif ico mov oga ogv png psd ra ram rgb flv',
      'max_filesize' => '0',
      'exclude_private' => TRUE,
    ],
    'extraction_result' => [
      // Default the configuration to a sensible amount of text to extract and
      // cache in the database. 1 million characters should be enough for most
      // cases.
      'number_first_bytes' => '1 MB',
    ],
  ];

  /**
   * {@inheritdoc}
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state): array {
    if ($form_state instanceof SubformStateInterface) {
      $form_state = $form_state->getCompleteFormState();
    }
    // Use temporary storage to get default values from parent form.
    $temporary_values = $form_state->getTemporary() + self::DEFAULT_EXTRACTION_SETTINGS;

    $form['extractable'] = [
      '#type' => 'details',
      '#title' => $this->t('Extractable'),
      '#description' => $this->t('Settings impacting if the file can be extracted or not.'),
      '#open' => TRUE,
    ];
    $form['extractable']['excluded_extensions'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Excluded file extensions'),
      '#description' => $this->t('File extensions that are excluded from extraction. Separate extensions with a space and do not include the leading dot.<br />Example: "aif art avi bmp gif ico mov oga ogv png psd ra ram rgb flv"<br />Extensions are internally mapped to a MIME type, so it is not necessary to put variations that map to the same type (e.g. tif is sufficient for tif and tiff)'),
      '#default_value' => $temporary_values['extractable']['excluded_extensions'],
      '#size' => (int) 80,
      '#maxlength' => (int) 255,
    ];
    $form['extractable']['max_filesize'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum file size'),
      '#description' => $this->t('Enter a value like "512" (bytes), "80 KB" (kilobytes) or "50 MB" (megabytes) in order to restrict the max file size of files that should be extracted.<br /> Enter "0" for no limit restriction.'),
      '#default_value' => $temporary_values['extractable']['max_filesize'],
      '#size' => (int) 10,
    ];
    $form['extractable']['exclude_private'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Exclude private files'),
      '#description' => $this->t('Check this checkbox if you want to exclude private files from being extracted.'),
      '#default_value' => $temporary_values['extractable']['exclude_private'],
    ];
    $form['extraction_result'] = [
      '#type' => 'details',
      '#title' => $this->t('Extraction result'),
      '#description' => $this->t('Settings impacting the result of the extraction.'),
      '#open' => TRUE,
    ];
    $form['extraction_result']['number_first_bytes'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Maximum size of the extracted string'),
      '#description' => $this->t('Enter a value like "512" (bytes), "80 KB" (kilobytes) or "50 MB" (megabytes) in order to restrict the max size of the content after extraction.<br /> Enter "0" for no limit restriction.'),
      '#default_value' => $temporary_values['extraction_result']['number_first_bytes'],
      '#size' => (int) 10,
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function validateConfigurationForm(array &$form, FormStateInterface $form_state): void {
    // Validate 'number_first_bytes'.
    /** @var string $form_state_value_number_first_byte */
    $form_state_value_number_first_byte = $form_state->getValue([
      'extraction_result',
      'number_first_bytes',
    ]);
    $number_first_bytes = \trim($form_state_value_number_first_byte);
    if (!Bytes::validate($number_first_bytes)) {
      $form_state->setError($form['extraction_result']['number_first_bytes'], $this->t('The "@name" option must contain a valid value. You may either leave the text field empty or enter a string like "512" (bytes), "80 KB" (kilobytes) or "50 MB" (megabytes).', [
        '@name' => $this->t('Maximum size of the extracted string'),
      ]));
    }

    // Validate 'max_filesize'.
    /** @var string $form_state_value_max_filesize */
    $form_state_value_max_filesize = $form_state->getValue([
      'extractable',
      'max_filesize',
    ]);
    $max_filesize = \trim($form_state_value_max_filesize);
    if (!Bytes::validate($max_filesize)) {
      $form_state->setError($form['extractable']['max_filesize'], $this->t('The "@name" option must contain a valid value. You may either leave the text field empty or enter a string like "512" (bytes), "80 KB" (kilobytes) or "50 MB" (megabytes).', [
        '@name' => $this->t('Maximum file size'),
      ]));
    }
  }

}
