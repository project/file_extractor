<?php

/**
 * @file
 * Install and update functions for the file_extractor module.
 */

declare(strict_types=1);

use Drupal\file_extractor\Form\SettingsForm;

/**
 * Update settings and "File Extractor: extracted file" formatter structure.
 */
function file_extractor_update_9101(): void {
  \Drupal::service('file_extractor.updater')->update9101();
}

/**
 * Remove key_value collection and update configuration. Use proper cache bin.
 */
function file_extractor_update_9102(): void {
  \Drupal::keyValue('file_extractor')->deleteAll();
  /** @var \Drupal\Core\Config\Config $file_extractor_settings */
  $file_extractor_settings = \Drupal::service('config.factory')->getEditable(SettingsForm::CONFIG_NAME);
  $file_extractor_settings->clear('cache_enable');
  $file_extractor_settings->clear('reserve_cache');
  $file_extractor_settings->save();
}
