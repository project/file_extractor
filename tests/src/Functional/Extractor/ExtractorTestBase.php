<?php

declare(strict_types=1);

namespace Drupal\Tests\file_extractor\Functional\Extractor;

use Drupal\Tests\BrowserTestBase;
use Drupal\Tests\file_extractor\Traits\FileExtractorTestTrait;

/**
 * Base class for extractor plugins functional tests.
 */
abstract class ExtractorTestBase extends BrowserTestBase {

  use FileExtractorTestTrait;

  /**
   * {@inheritdoc}
   */
  protected $defaultTheme = 'stark';

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'file_extractor',
    'system',
    'user',
  ];

}
