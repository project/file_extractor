<?php

declare(strict_types=1);

namespace Drupal\Tests\file_extractor\Functional\Extractor;

use Drupal\file_extractor\Plugin\file_extractor\Extractor\TikaServerExtractor;
use Symfony\Component\Process\Process;

/**
 * Tests the "Tika server" extractor.
 *
 * Need to use functional tests for Tika server because of the need of an HTTP
 * client.
 *
 * @group file_extractor
 */
class TikaServerExtractorTest extends ExtractorTestBase {

  public const TIKA_SERVER_VERSION = '1.28.5';

  /**
   * {@inheritdoc}
   */
  protected $extractorPluginId = 'tika_server_extractor';

  /**
   * {@inheritdoc}
   */
  protected $extractorPluginConfiguration = [
    'scheme' => 'http',
    'host' => 'localhost',
    'port' => TikaServerExtractor::TIKA_SERVER_DEFAULT_PORT,
    'timeout' => TikaServerExtractor::DEFAULT_TIMEOUT,
  ];

  /**
   * The process running the Tika server.
   *
   * @var \Symfony\Component\Process\Process
   */
  protected $process;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // Launch Tika server in a dedicated process.
    $this->process = new Process([
      'java',
      '-jar',
      '/var/apache-tika/tika-server-' . self::TIKA_SERVER_VERSION . '.jar',
    ]);
    $this->process->start();
    // Need to ensure that the Tika server is started.
    \sleep(5);
  }

  /**
   * {@inheritdoc}
   */
  protected function tearDown(): void {
    $this->process->stop();
    parent::tearDown();
  }

}
