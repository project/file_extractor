<?php

declare(strict_types=1);

namespace Drupal\Tests\file_extractor\Kernel\Extractor;

use Drupal\KernelTests\KernelTestBase;
use Drupal\Tests\file_extractor\Traits\FileExtractorTestTrait;

/**
 * Base class for extractor plugins kernel tests.
 */
abstract class ExtractorTestBase extends KernelTestBase {

  use FileExtractorTestTrait;

  /**
   * {@inheritdoc}
   */
  protected static $modules = [
    'file',
    'file_extractor',
    'system',
    'user',
  ];

}
