<?php

declare(strict_types=1);

namespace Drupal\Tests\file_extractor\Kernel\Extractor;

/**
 * Tests the "Python Pdf2txt" extractor.
 *
 * @group file_extractor
 */
class PythonPdf2txtExtractorTest extends ExtractorTestBase {

  /**
   * {@inheritdoc}
   */
  protected $extractorPluginId = 'python_pdf2txt_extractor';

  /**
   * {@inheritdoc}
   */
  protected $extractorPluginConfiguration = [
    'python_path' => 'python3',
    'python_pdf2txt_script' => '/usr/bin/pdf2txt',
  ];

}
