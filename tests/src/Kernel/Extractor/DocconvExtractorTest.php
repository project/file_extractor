<?php

declare(strict_types=1);

namespace Drupal\Tests\file_extractor\Kernel\Extractor;

/**
 * Tests the "Docconv" extractor.
 *
 * @group file_extractor
 */
class DocconvExtractorTest extends ExtractorTestBase {

  /**
   * {@inheritdoc}
   */
  protected $extractorPluginId = 'docconv_extractor';

  /**
   * {@inheritdoc}
   */
  protected $extractorPluginConfiguration = [
    'docconv_path' => '/usr/bin/docd',
  ];

}
