<?php

declare(strict_types=1);

namespace Drupal\Tests\file_extractor\Kernel\Extractor;

/**
 * Tests the "Pdftotext" extractor.
 *
 * @group file_extractor
 */
class PdftotextExtractorTest extends ExtractorTestBase {

  /**
   * {@inheritdoc}
   */
  protected $extractorPluginId = 'pdftotext_extractor';

  /**
   * {@inheritdoc}
   */
  protected $extractorPluginConfiguration = [
    'pdftotext_path' => 'pdftotext',
  ];

}
