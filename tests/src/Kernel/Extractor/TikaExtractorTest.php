<?php

declare(strict_types=1);

namespace Drupal\Tests\file_extractor\Kernel\Extractor;

/**
 * Tests the "Tika" extractor.
 *
 * @group file_extractor
 */
class TikaExtractorTest extends ExtractorTestBase {

  public const TIKA_APP_VERSION = '1.28.5';

  public const TIKA_CONFIG_FILE_PATH = '/tmp/test-tika-config.xml';

  /**
   * {@inheritdoc}
   */
  protected $extractorPluginId = 'tika_extractor';

  /**
   * {@inheritdoc}
   */
  protected $extractorPluginConfiguration = [
    'java_path' => 'java',
    'tika_path' => '/var/apache-tika/tika-app-' . self::TIKA_APP_VERSION . '.jar',
    'tika_config_path' => self::TIKA_CONFIG_FILE_PATH,
  ];

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    // cSpell:disable.
    // Generate Tika config file.
    $tika_config = '
      <properties>
        <service-loader initializableProblemHandler="ignore"/>
      </properties>
    ';
    // cSpell:enable.
    \file_put_contents(self::TIKA_CONFIG_FILE_PATH, $tika_config);
  }

}
