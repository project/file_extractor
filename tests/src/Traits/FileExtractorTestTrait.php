<?php

declare(strict_types=1);

namespace Drupal\Tests\file_extractor\Traits;

/**
 * Avoid code duplication between test base classes.
 */
trait FileExtractorTestTrait {

  /**
   * Test file helper service.
   *
   * @var \Drupal\file_extractor\Service\TestFileHelperInterface
   */
  protected $testFileHelper;

  /**
   * The tested extractor plugin ID.
   *
   * @var string
   */
  protected $extractorPluginId;

  /**
   * The tested extractor plugin configuration.
   *
   * @var array
   */
  protected $extractorPluginConfiguration = [];

  /**
   * The tested extractor.
   *
   * @var \Drupal\file_extractor\Extractor\ExtractorPluginInterface
   */
  protected $extractor;

  /**
   * {@inheritdoc}
   */
  protected function setUp(): void {
    parent::setUp();

    $this->testFileHelper = $this->container->get('file_extractor.test_file_helper');

    /** @var \Drupal\file_extractor\Extractor\ExtractorPluginManager $extractor_plugin_manager */
    $extractor_plugin_manager = $this->container->get('plugin.manager.file_extractor.extractor');
    /** @var \Drupal\file_extractor\Extractor\ExtractorPluginInterface $extractor_plugin */
    $extractor_plugin = $extractor_plugin_manager->createInstance($this->extractorPluginId, $this->extractorPluginConfiguration);
    $this->extractor = $extractor_plugin;
  }

  /**
   * Tests extraction method.
   */
  public function testExtraction(): void {
    $extraction = $this->extractor->extract($this->testFileHelper->getTestFile());
    $this->assertNotEmpty($extraction, 'The extraction with the plugin ' . $this->extractorPluginId . ' is not working.');
  }

}
