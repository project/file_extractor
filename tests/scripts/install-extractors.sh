#!/bin/bash

apt-get update

# Docconv.
rm -rf /usr/local/go
rm -rf /root/go
GO_VERSION='1.21.6'
DOCCONV_VERSION='v1.3.8'
apt-get install curl poppler-utils wv unrtf tidy -y
curl -OJL https://go.dev/dl/go${GO_VERSION}.linux-amd64.tar.gz
tar -C /usr/local -xzf go${GO_VERSION}.linux-amd64.tar.gz
chown -R root:root /usr/local/go
/usr/local/go/bin/go install code.sajari.com/docconv/docd@${DOCCONV_VERSION}

cd /root/go/pkg/mod/code.sajari.com/docconv@${DOCCONV_VERSION}/docd
/usr/local/go/bin/go build .
cd -
cp /root/go/pkg/mod/code.sajari.com/docconv@${DOCCONV_VERSION}/docd/docd /usr/bin/

# Pdftotext.
apt-get install poppler-utils -y

# Python Pdf2txt.
apt-get install python3-pdfminer -y

# Tika.
TIKA_VERSION='1.28.5'
mkdir -p /usr/share/man/man1
mkdir -p /var/apache-tika
apt-get install curl default-jre -y
curl https://archive.apache.org/dist/tika/${TIKA_VERSION}/tika-app-${TIKA_VERSION}.jar -o /var/apache-tika/tika-app-${TIKA_VERSION}.jar

# Tika server.
TIKA_SERVER_VERSION='1.28.5'
mkdir -p /usr/share/man/man1
mkdir -p /var/apache-tika
apt-get install curl default-jre -y
curl https://archive.apache.org/dist/tika/${TIKA_SERVER_VERSION}/tika-server-${TIKA_SERVER_VERSION}.jar -o /var/apache-tika/tika-server-${TIKA_SERVER_VERSION}.jar
chmod 777 /var/apache-tika/tika-server-${TIKA_SERVER_VERSION}.jar
#java -jar /var/apache-tika/tika-server-${TIKA_SERVER_VERSION}.jar
